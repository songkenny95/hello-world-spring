package com.example.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(GetVowelCount.class)
public class GetVowelCountTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    void GetVowelCount_noArgs_returnsZero() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/getVowelCount"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("0") );
    }

    @Test
    void GetVowelCount_singleVowel_returnsOne() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/getVowelCount?word=a"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("1") );
    }

    @Test
    void GetVowelCount_singleConsonant_returnsZero() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/getVowelCount?word=s"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("0") );
    }

    @Test
    void GetVowelCount_word_returnsVowelTotal() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/getVowelCount?word=storage"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("3") );
    }
}
