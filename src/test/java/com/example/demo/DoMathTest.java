package com.example.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(DoMath.class)
public class DoMathTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    void DoMath_noArgs_returnsZero() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/doMath"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("0") );
    }

    @Test
    void DoMath_oneNum_returnsNum() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/doMath?nums=1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("1") );
    }

    @Test
    void DoMath_twoNumAdd_returnsNum() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/doMath?nums=3,1&ops=+"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("4") );
    }

    @Test
    void DoMath_twoNumSubtract_returnsNum() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/doMath?nums=3,1&ops=-"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("2") );
    }

    @Test
    void DoMath_twoNumMultiply_returnsNum() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/doMath?nums=3,4&ops=*"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("12") );
    }

    @Test
    void DoMath_twoNumDivide_returnsNum() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/doMath?nums=8,2&ops=/"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("4") );
    }

    @Test
    void DoMath_allOperatorsTogether_returnsNum() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/doMath?nums=8,2,5,3,6&ops=/,+,-,*"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("36") );
    }

    @Test
    void DoMath_moreNumsThanOps_returnsNumUpToLastValidNum() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/doMath?nums=8,2,5,3,6&ops=+"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("10") );
    }

    @Test
    void DoMath_moreOpsThanNums_returnsNumUpToLastOperator() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/doMath?nums=8,6&ops=-,*"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("2") );
    }
}
