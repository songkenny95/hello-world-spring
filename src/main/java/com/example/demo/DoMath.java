package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class DoMath {
    @GetMapping("/doMath")
    public String sayHello(@RequestParam(required = false) ArrayList<Integer> nums, @RequestParam (required = false) ArrayList<Character> ops) {
        if (nums == null) {
            return "0";
        } else if (nums.size() == 1){
            return nums.get(0).toString();
        }
        Integer total = nums.get(0);
        int maxLen = Math.min(ops.size(), nums.size() - 1);
        for (int i = 0; i < maxLen; i++) {
            if (ops.get(i) == '+') {
                total += nums.get(i + 1);
            } else if (ops.get(i) == '-') {
                total -= nums.get(i + 1);
            } else if (ops.get(i) == '*') {
                total *= nums.get(i + 1);
            } else if (ops.get(i) == '/') {
                total /= nums.get(i + 1);
            }
        }
        return total.toString();
    }
}
