package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashSet;

@RestController
public class GetVowelCount {
    private HashSet<Character> vowels = new HashSet<>(){{
        add('a');
        add('e');
        add('i');
        add('o');
        add('u');
    }};

    @GetMapping("/getVowelCount")
    public String sayHello(@RequestParam(defaultValue = "", required = false) String word) {
        Integer total = 0;
        for (int i = 0; i < word.length(); i++) {
            if (vowels.contains(word.charAt(i))) {
                total++;
            }
        }
        return total.toString();
    }
}
